# Ejercicios

## Ejercicios básicos

### Ejercicio 1

Añadir un atributo nuevo al Producto para indicar el año de fabricación de este.

> Se debe pensar el nombre del atributo.
> Siempre en inglés.
> Debe ser de tipo entero
```
El tipo de dato se llama "Product".
```

Añadir valor a este atributo en los impex de productos

> No es necesario añadirle valor a todos los productos, sino simplemente a un subconjunto pequeño de estos. Por ejemplo, a 10 productos.

```
El impex se llama "products.impex".
```

Mostrar el atributo en la Página Detalle de Producto (conocida como PDP)

> Para esto es necesario realizar varias modificaciones: 
> * Modificar el Data del producto (ProductData) desde el XML
> * Modificar un populator de producto existente (ProductBasicPopulator) para añadir al data el valor del modelo
> * Modificar el frontend de forma sencilla para que este atributo se muestre
```
El archivo a modificar para mostrar el atributo en el frontend se encuentra en la siguiente ruta: `trainingstorefront/web/webroot/WEB-INF/tags/responsive/product/productDetailsPanel.tag`
Se debe añadir un `<div>...</div>` nuevo, justo debajo de `<div class="description">`, que contenga el dato a mostrar
```

### Ejercicio 2

Mostrar el atributo del ejercicio anterior en la página de resultados de búsqueda (conocida como PGP - Página de Grid de Producto - o PLP - Página de Listado de Producto - ). 

> Para esto se debe seguir los siguientes pasos:
> * Añadir una propiedad indexada en el fichero de solr.impex, dentro de la sección marcada con el comentario `# Non-facet properties`.
> * Esta propiedad debe llamarse igual que el atributo añadido en el ejercicio anterior.
> * Para que se cargue el dato que se acaba de insertar en el fichero impex se debe realizar un `update` o `initialize`, pero hay formas más rápidas para este caso sencillo.
>   Se puede, simplemente, cargar los impex manualmente desde el `HAC`. Para ello se deben seguir los siguientes pasos:
>   * Ir al `HAC` y loguearse con el usuario `admin` (https://training.local:9002/hac).
>   * Ir a `Console > ImpEx Import`.
>   * Copiar el fichero ImpEx que se quiera cargar y pegarlo en el campo de texto.
>   * Pulsar el botón `Importar contenido` y esperar a que indique que ha terminado correctamente.
> * Modificar el fichero `productListerItem.tag` para añadir el nuevo atributo indexado
>   * Se recomienda añadir un `<div>...</div>` justo debajo de `<div class="product__listing--description">`
> * Para tener el valor en solr, y por tanto que aparezca al buscar, se debe realizar una indexación, para ello hay que seguir los siguientes pasos:
>   * Abrir backoffice y loguearse con el usuario `admin`
>   * Ir a backoffice y navegar a la sección: `Sistema > Búsqueda y navegación > Configuración de búsqueda de facetas`
>   * Seleccionar `trainingIndex`
>   * En el detalle, seleccionar la acción `Índice`
>   * En el popup seleccionar `Full` y pulsar sobre el botón `Inicio`

### Ejercicio 3

Añadir el atributo también como filtro (facet) de búsqueda.

> Para esto se deben realizar los siguientes pasos:
> * Añadir una propiedad indexada nueva en solr.impex, pero esta vez como facet, dentro de la sección marcada como `# Other facet properties`.
> * Esta propiedad queremos que no se llame igual que la del ejercicio anterior (Aún que en un caso real podría ser la misma), y esto es un problema ya que no obtendrá el valor automáticamente.
>   Para ello se debe crear un `ValueResolver` (Es una forma alternativa a los ValueProviders y funcionan de forma similar).
>   Para crearlo se puede basar en uno ya existente, como por ejemplo: `ProductPricesValueResolver`. No hay que olvidarse de crear también el bean.
>   Se debe crear dentro de `trainingcore`.
> * No debe ser multivalor pero sí que debe ser `MultiSelectOr`, de forma que se puedan seleccionar varios valores unidos con un `OR`.
> * Realizar una carga del fichero vía `HAC` (como en el ejercicio anterior).
> * Por último, realizar una indexación (como en el ejercicio anterior).
> * Recordad que hay que añadir también las localizaciones de los atributos indexados que se añadan al `solr.impex` como facets.
    Este valor será el título del facet que se acaba de crear.
    Estos impex también habrá que cargarlos vía `HAC` (cada idioma por separado).

## Challenge básico

## Challenge básico (parte 1)

El challenge es el ejercicio final, sin ayuda detallada. 
A continuación se explica la problemática y se aporta una serie de ayudas pero sin entrar en detalles.
Se permite realizar preguntas.

Se requiere que los productos tengan una lista de accesorios. 
Estos accesorios no serán otros productos, sino que se creará un tipo de dato nuevo para ello:
* Este tipo de dato debe tener un código (que se añadirá de forma manual), un nombre (localizado), un precio (debe poder tener un precio por tipo de moneda) y una única imagen principal.
* Para crear la lista de accesorios en el producto se debe realizar una relación N-N con ambos modelos.

Se debe modificar la página de detalle de producto para añadir un botón que lleve a la nueva página que se detalla a continuación).

Se requiere crear una página nueva donde poder ver todos los accesorios en forma de listado (o tabla).
Esta página debe estar integrada en la página, es decir, debe tener cabecera y footer igual que el resto de páginas.
Para ello se debe crear todos los tipos de artefactos de hybris:
* Controller
* Facade
* Service
* Dao
* Converter
* Populator
* Además de crear el JSP y la página CMS (Se puede solicitar ayuda adicional para este apartado).

Se pide añadir algunos accesorios vía ImpEx, y relacionarlos con algunos productos.

Y por último, customizar backoffice para poder gestionar los accesorios.
Para ello se debe añadir los siguientes componentes a la configuración de backoffice:
* Explorer-tree
* Listview
* Editor-area
* Simple-search
* Advanced-search
* Base
* Create-wizard

### Challenge básico (parte 2)

Se requiere que se pueda buscar por el **nombre** de los accesorios. Esto quiere decir que si se busca el accesorio con nombre "X", deben aparecer como resultado de búsqueda todos los productos que contengan este accesorio.
Además, se quiere tener como facet de búsqueda de forma que se puedan seleccionar varios accesorios y que se muestren los productos que contengan TODOS los accesorios seleccionados (no únicamente uno de ellos).

### Challenge básico (parte 3)
Se quiere que a los accesorios se les asigne un código único de forma automática e incremental, con el patrón `AC-#########` ("AC-" seguido de 9 dígitos numéricos).

Para ello se debe crear un `Interceptor` y un `Key Generator`.
Para este ejercicio se debe indagar por hybris para encontrar la forma de realizarlo con las dos pistas que se han dado anteriormente.

## Ejercicios avanzados

### Ejercicio 1
Creación de un CronJob que elimine o cambie el estado de los productos de la versión de catálogo staged en función del valor del atributo `statusType`.

> * Crear un nuevo `enumtype StatusType` con los siguientes valores: `TO_REMOVE` y `TO_DISAPPROVE`.
> * Crear un nuevo atributo en el `itemtype Product` llamado `statusType` del tipo `StatusType` debe ser opcional y por defecto no debe llevar ningún valor asociado.
> * Crear un cronJob que: 
>   * Se debe ejecutar automáticamente todos los días a la 1:30 AM.
>   * Debe eliminar los productos si el atributo `statusType` es igual a `TO_REMOVE` (pista: consultad la interfaz `ModelService`).
>   * Debe settear el valor del atributo `approvalStatus` del producto al valor `unapproved` a aquellos productos cuyo `statusType` sea igual a `TO_DISAPPROVE`.

### Ejercicio 2

Añadir un atributo dinámico que indique los años que tiene un producto según su año de fabricación.
Se requiere, además, que se pueda modificar este valor y al indicar un valor válido, se actualize el año de fabricación.
```
Por ejemplo:
* Año de fabricación: 2011.
* Año actual: 2021.
* Atributo dinámico debe indicar: 10.
* Si se modifica este valor, a través de backoffice, e indicamos 20, el año de fabricación debe cambiar a 2001. Ya que 2021 - 20 = 2001.
```

## Challenge avanzado

### Challenge avanzado (parte 1)

Se requiere que, cada vez que el usuario acceda a la página de accesorios, pueda seleccionar algunos de ellos.
Cada vez que se marque o desmarque alguno, se debe sumar su precio y mostrarlo de forma clara en la página (Se debe tener en cuenta la moneda del usuario).

Debe existir un botón que indique `guardar`. De forma que cuando se pulse, guarde la selección junto al producto para el usuario activo.
Se debe redireccionar al usuario a la misma página de accesorios junto con un mensaje informando que se ha guardado la selección de accesorios.

Cuando el usuario acceda a la página de carrito, junto a cada producto debe poder visualizar los accesorios seleccionados y el precio adicional, no teniendolo en cuenta para la suma del total, 
pero sí que debe mostrarse un `Total de accesorios`, el cual sumará todos los accesorios de todos los productos.

### Challenge avanzado (parte 2)

Añadir al correo de confirmación los accesorios marcados en el pedido realizado.
Se debe tener en cuenta que cuando se mande el correo, es posible que el usuario haya cambiado los accesorios de los productos, por lo que debe guardarse en el propio pedido.


