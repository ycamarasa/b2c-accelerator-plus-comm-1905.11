# Frontend Training

## Pages & Components

### Crear una página

Para crear una página nueva se deben definir los siguientes elementos:

* PageTemplate (Si se requiere)
* ContentSlot
* ContentSlotName
* ContentSlotForTemplate
* ContentPage / ProductPage / CategoryPage
* JSP
*

```impex
INSERT_UPDATE PageTemplate; $contentCV[unique = true]; uid[unique = true]; name               ; frontendTemplateName; restrictedPageTypes(code); active[default = true]
                          ;                          ; TestPageTemplate  ; Test Page Template ; test/testLayoutPage ; ContentPage              ; true
```

El atributo `frontendTemplateName` indica la ruta donde se encuentra el archivo jsp a partir de la dirección
`trainingstorefront/web/webroot/WEB-INF/views/responsive/pages/`.

```impex
UPDATE PageTemplate; $contentCV[unique = true]; uid[unique = true]; velocityTemplate[translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
                   ;                          ; TestPageTemplate  ; $jarResourceCms/structure-view/structure_testPageTemplate.vm
```

> Ejemplo de `structure_testPageTemplate.vm`
>
> ```html
> <div>
>     <table width="100%" cellspacing="0" style="margin:0;padding:0;table-layout:auto;border:1px solid #1E4EBF;">
>         <tbody>
>         <tr>
>             <td colspan="4" class="structureViewSection">
>                 <cockpit code="TopHeaderSlot"/>
>             </td>
>         </tr>
>         <tr>
>             <td height="125px" width="35%" rowspan="2" colspan="2" class="structureViewSection">
>                 <cockpit code="SiteLogo"/>
>             </td>
>             <td class="structureViewSection">
>                 <cockpit code="HeaderLinks"/>
>                 <cockpit code="SearchBox"/>
>             </td>
>             <td width="20%" rowspan="2" class="structureViewSection">
>                 <cockpit code="MiniCart"/>
>             </td>
>         </tr>
>         <tr>
>             <td colspan="4" class="structureViewSection">
>                 <cockpit code="NavigationBar"/>
>             </td>
>         </tr>
>         <tr>
>             <td colspan="4" class="structureViewSection">
>                 <cockpit code="BottomHeaderSlot"/>
>             </td>
>         </tr>
>         <tr>
>             <td height="543px" width="40%" colspan="1" class="structureViewSection" style="vertical-align:middle;">
>                 <div>Product</div>
>             </td>
>             <td width="20%" class="structureViewSection" style="vertical-align:top;">
>                 <cockpit code="VariantSelector"/>
>                 <cockpit code="AddToCart"/>
>             </td>
>             <td width="20%" class="structureViewSection" style="vertical-align:top;">
>                 <cockpit code="CrossSelling"/>
>             </td>
>             <td width="20%" class="structureViewSection" style="vertical-align:top;">
>                 <cockpit code="Wishlists"/>
>             </td>
>         </tr>
>         <tr>
>             <td height="267px" width="20%" class="structureViewSection" style="vertical-align:top;">
>                 <cockpit code="UpSelling"/>
>             </td>
>             <td class="structureViewSection" colspan="3" style="vertical-align:top;">
>                 <cockpit code="Tabs"/>
>             </td>
>         </tr>
>         <tr>
>             <td height="270px" colspan="4" class="structureViewSection">
>                 <cockpit code="Footer"/>
>             </td>
>         </tr>
>         </tbody>
>     </table>
>     <div style="width:100%; border-top: 2px solid #bbb" class="cmsContentEditor">
>         <cockpit code="editor"/>
>     </div>
> </div>
> ```

ContentSlotName para el Template, teniendo en cuenta el fichero `vm` creado anteriormente:

```impex
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'TestPageTemplate']; validComponentTypes(code)       ; compTypeGroup(code)
                             ; SiteLogo           ;                                                                       ;                                 ; logo
                             ; HeaderLinks        ;                                                                       ;                                 ; headerlinks
                             ; SearchBox          ;                                                                       ;                                 ; searchbox
                             ; MiniCart           ;                                                                       ;                                 ; minicart
                             ; NavigationBar      ;                                                                       ;                                 ; navigation
                             ; VariantSelector    ;                                                                       ; ProductVariantSelectorComponent ; narrow
                             ; AddToCart          ;                                                                       ; ProductAddToCartComponent       ; narrow
                             ; CrossSelling       ;                                                                       ; ProductReferencesComponent      ; narrow
                             ; Wishlists          ;                                                                       ; WishlistListComponent           ; narrow
                             ; UpSelling          ;                                                                       ; ProductReferencesComponent      ; narrow
                             ; Footer             ;                                                                       ;                                 ; footer
                             ; Tabs               ;                                                                       ; CMSTabParagraphContainer        ;
                             ; TopHeaderSlot      ;                                                                       ;                                 ; wide
                             ; BottomHeaderSlot   ;                                                                       ;                                 ; wide
```

Ahora se crean los ContentSlot para el Template:

```impex
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]  ; name                   ; active
                         ;                          ; SiteLogoSlot        ; Default Site Logo Slot ; true
                         ;                          ; MiniCartSlot        ; Mini Cart              ; true
                         ;                          ; NavigationBarSlot   ; Navigation Bar         ; true
                         ;                          ; TabsSlot            ; Tabs                   ; true
                         ;                          ; FooterSlot          ; Footer                 ; true
                         ;                          ; HeaderLinksSlot     ; Header links           ; true
                         ;                          ; SearchBoxSlot       ; Search Box             ; true
                         ;                          ; VariantSelectorSlot ; Variant Selector       ; true
                         ;                          ; AddToCartSlot       ; Add To Cart            ; true
                         ;                          ; UpSellingSlot       ; Up Selling             ; true
                         ;                          ; CrossSellingSlot    ; Cross Selling          ; true
                         ;                          ; TopHeaderSlot       ; Top Header             ; true
                         ;                          ; BottomHeaderSlot    ; Bottom Header          ; true
```

Ahora se debe enlazar los ContentSlot al Template

```impex
INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]               ; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'TestPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
                                    ;                          ; SiteLogo-ProductWishlist         ; SiteLogo               ;                                                                           ; SiteLogoSlot                               ; true
                                    ;                          ; HomepageLink-ProductWishlist     ; HomepageNavLink        ;                                                                           ; HomepageNavLinkSlot                        ; true
                                    ;                          ; MiniCart-ProductWishlist         ; MiniCart               ;                                                                           ; MiniCartSlot                               ; true
                                    ;                          ; NavigationBar-ProductWishlist    ; NavigationBar          ;                                                                           ; NavigationBarSlot                          ; true
                                    ;                          ; Tabs-ProductWishlist             ; Tabs                   ;                                                                           ; TabsSlot                                   ; true
                                    ;                          ; Footer-ProductWishlist           ; Footer                 ;                                                                           ; FooterSlot                                 ; true
                                    ;                          ; HeaderLinks-ProductWishlist      ; HeaderLinks            ;                                                                           ; HeaderLinksSlot                            ; true
                                    ;                          ; SearchBox-ProductWishlist        ; SearchBox              ;                                                                           ; SearchBoxSlot                              ; true
                                    ;                          ; VariantSelector-ProductWishlist  ; VariantSelector        ;                                                                           ; VariantSelectorSlot                        ; true
                                    ;                          ; AddToCart-ProductWishlist        ; AddToCart              ;                                                                           ; AddToCartSlot                              ; true
                                    ;                          ; UpSelling-ProductWishlist        ; UpSelling              ;                                                                           ; UpSellingSlot                              ; true
                                    ;                          ; CrossSelling-ProductWishlist     ; CrossSelling           ;                                                                           ; CrossSellingSlot                           ; true
                                    ;                          ; TopHeaderSlot-ProductWishlist    ; TopHeaderSlot          ;                                                                           ; TopHeaderSlot                              ; true
                                    ;                          ; BottomHeaderSlot-ProductWishlist ; BottomHeaderSlot       ;                                                                           ; BottomHeaderSlot                           ; true
```

> Ejemplo de `testLayoutPage.jsp`
>
> ```html
> <%@ page trimDirectiveWhitespaces="true" %>
> <%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
> <%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %> 
> <%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %> 
> <%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %> 
> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
> <template:page pageTitle="${pageTitle}">
>     <div id="globalMessages">
>         <common:globalMessages/>
>     </div>
>     <product:productDetailsPanel product="${product}" galleryImages="${galleryImages}"/>
>     <cms:pageSlot position="CrossSelling" var="comp" element="div" class="span-24">
>         <cms:component component="${comp}"/>
>     </cms:pageSlot>
>     <!-- START CUSTOM -->
>     <cms:pageSlot position="Wishlists" var="comp" element="div" class="span-24">
>         <cms:component component="${comp}"/>
>     </cms:pageSlot>
>     <!-- END CUSTOM -->
>     <cms:pageSlot position="UpSelling" var="comp" element="div" class="span-24">
>         <cms:component component="${comp}"/>
>     </cms:pageSlot>
> </template:page>
> ```

Ahora se debe crear la ContentPage:

```impex
INSERT_UPDATE ContentPage; $contentCV[unique = true]; uid[unique = true]; name      ; masterTemplate(uid, $contentCV); label    ; defaultPage[default = 'true']; approvalStatus(code)[default = 'approved']; homepage[default = 'false']; previewImage(code, $contentCV)[default = 'ContentPageModel__function_preview']
                         ;                          ; testContentPage   ; Test Page ; TestPageTemplate               ; testPage ;
```

También se debe añadir el título para cada idioma:

```impex
$contentCatalog = jdb2cContentCatalog
$contentCV = catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]), CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]

# Language 
$lang = en

# Content Pages 
UPDATE ContentPage; $contentCV[unique = true]; uid[unique = true]; title[lang = $lang]
                  ;                          ; testContentPage   ; "Test Page"
```

A continuación se crean los ContentSlot para la página y se enlazan con esta (`WishlistList` es un componente custom que se definirá posteriormente):

```impex
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name                       ; active; cmsComponents(&componentRef)
                         ;                          ; WishlistListSlot  ; Wishlist List Content Slot ; true  ; WishlistList

INSERT_UPDATE ContentSlotForPage; $contentCV[unique = true]; uid[unique = true]        ; position[unique = true]; page(uid, $contentCV)[unique = true][default = 'testContentPage']; contentSlot(uid, $contentCV)[unique = true]
                                ;                          ; Wishlists-TestContentPage ; Wishlists              ;                                                                  ; WishlistListSlot
```

Ahora se van a crear los componentes para, posteriormente, enlazarlos a los ContentSlot.
> Es posible que algunos de estos componentes ya existan y, por lo tanto, no es necesario crearlos de nuevo

```impex
INSERT_UPDATE SimpleBannerComponent; $contentCV[unique = true]; uid[unique = true]; name                ; &componentRef     ; urlLink
                                   ;                          ; SiteLogoComponent ; Site Logo Component ; SiteLogoComponent ; "/"

INSERT_UPDATE CMSLinkComponent; $contentCV[unique = true]; uid[unique = true]; name               ; url; &linkRef        ; &componentRef   ; target(code)[default = 'sameWindow']; $category; $product;
                              ;                          ; HomepageNavLink   ; Home Page Nav Link ; /  ; HomepageNavLink ; HomepageNavLink ;                                     ;          ;         ;

INSERT_UPDATE MiniCartComponent; $contentCV[unique = true]; uid[unique = true]; name      ; &componentRef; totalDisplay(code); shownProductCount; lightboxBannerComponent(&componentRef)
                               ;                          ; MiniCart          ; Mini Cart ; MiniCart     ; SUBTOTAL          ; 3                ; LightboxHomeDeliveryBannerComponent

INSERT_UPDATE NavigationBarCollectionComponent; $contentCV[unique = true]; uid[unique = true]; name                                ; components(uid, $contentCV)            ; &componentRef
                                              ;                          ; NavBarComponent   ; Navigation Bar Collection Component ; StuffBarComponent, ClothesBarComponent ; NavBarComponent

INSERT_UPDATE BreadcrumbComponent; $contentCV[unique = true]; uid[unique = true]  ; name                     ; &componentRef
                                 ;                          ; breadcrumbComponent ; Breadcrumb CMS Component ; breadcrumbComponent

INSERT_UPDATE CMSTabParagraphContainer; $contentCV[unique = true]; uid[unique = true]; name          ; visible; simpleCMSComponents(uid, $contentCV); &componentRef
                                      ;                          ; TabPanelContainer ; Tab container ; true   ; deliveryTab                         ; TabPanelContainer

INSERT_UPDATE FooterComponent; $contentCV[unique = true]; uid[unique = true]; wrapAfter; &componentRef
                             ;                          ; FooterComponent   ; 2        ; FooterComponent

INSERT_UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]; name                ; &componentRef
                                   ;                          ; ContactInfo       ; Contact information ; ContactInfo

INSERT_UPDATE SearchBoxComponent; $contentCV[unique = true]; uid[unique = true]; name       ; &componentRef
                                ;                          ; SearchBox         ; Search Box ; SearchBox

INSERT_UPDATE LanguageCurrencyComponent; $contentCV[unique = true]; uid[unique = true]    ; name                            ; &componentRef
                                       ;                          ; LangCurrencyComponent ; Language and Currency Component ; LangCurrencyComponent

INSERT_UPDATE ProductVariantSelectorComponent; $contentCV[unique = true]; uid[unique = true]; name                     ; &componentRef
                                             ;                          ; VariantSelector   ; Product Variant Selector ; VariantSelector

INSERT_UPDATE ProductAddToCartComponent; $contentCV[unique = true]; uid[unique = true]; name                ; actions(&actionRef)                                              ; &componentRef
                                       ;                          ; AddToCart         ; Product Add To Cart ; PickUpInStoreAction, AddToCartAction, ShareOnSocialNetworkAction ; AddToCart

INSERT_UPDATE ProductReferencesComponent; $contentCV[unique = true]; uid[unique = true]; name        ; productReferenceTypes(code); maximumNumberProducts; &componentRef
                                        ;                          ; Similar           ; Similar     ; SIMILAR                    ; 5                    ; Similar
                                        ;                          ; accessories       ; Accessories ; ACCESSORIES                ; 5                    ; accessories
```

```impex
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]  ; cmsComponents(&componentRef)
                         ;                          ; SiteLogoSlot        ; SiteLogoComponent
                         ;                          ; HomepageNavLinkSlot ; HomepageNavLink
                         ;                          ; MiniCartSlot        ; MiniCart
                         ;                          ; NavigationBarSlot   ; NavBarComponent , breadcrumbComponent
                         ;                          ; TabsSlot            ; TabPanelContainer
                         ;                          ; FooterSlot          ; FooterComponent
                         ;                          ; HeaderLinksSlot     ; ContactInfo
                         ;                          ; SearchBoxSlot       ; SearchBox , LangCurrencyComponent
                         ;                          ; VariantSelectorSlot ; VariantSelector
                         ;                          ; AddToCartSlot       ; AddToCart
                         ;                          ; UpSellingSlot       ; Similar
                         ;                          ; CrossSellingSlot    ; accessories
``` 

#### Crear un componente nuevo

También se debe crear el componente custom que se está creando:

Primero se debe definir el itemtype dentro del grupo de "Components":

```xml

<typegroup name="Components">

    <itemtype code="WishlistListComponent" autocreate="true" generate="true" extends="SimpleCMSComponent"
              jaloclass="de.hybris.training.core.jalo.cms2.WishlistListComponent">
        <description>Displays a list of Wishlists.</description>
        <attributes>
            <attribute qualifier="title" type="localized:java.lang.String">
                <persistence type="property"/>
                <modifiers/>
                <description>Localized title of the component.</description>
            </attribute>
        </attributes>
    </itemtype>

</typegroup>
```

También se le debe poner un nombre al tipo de dato:

```properties
type.WishlistListComponent.name=Wishlist List Component
```

Se crea el JSP del componente:
```html
<%@ page trimDirectiveWhitespaces="true" %> 
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %> 
<%@ taglib prefix="createWishlist" tagdir="/WEB-INF/tags/desktop/wishlist" %> 
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %> 
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="testPagePanel">
    <p>Example Test Page: ${title}</p>
    <p>--${exampleExtraAttribute}--</p>
</div>
```

Se puede crear un Controlador en el caso que el Componente deba mostrar datos dinámicos:

Primero se debe añadir en las constantes del storefront lo siguiente (Añadir lo que falte):
> trainingstorefront/web/src/de/hybris/training/storefront/controllers/ControllerConstants.java
```java
public interface ControllerConstants {
interface Actions {
        interface Cms {
            String WishlistListComponent = _Prefix + WishlistListComponentModel._TYPECODE + _Suffix;
        }
    }
}
```

```java
package de.hybris.training.storefront.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import de.hybris.training.core.model.WishlistListComponent;
import de.hybris.training.storefront.controllers.ControllerConstants;

@Controller("WishlistListComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.WishlistListComponent)
public class WishlistListComponentController extends AbstractCMSComponentController<WishlistListComponentModel> {
    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final WishlistListComponentModel component) {
        model.addAttribute("title", component.getTitle());
        model.addAttribute("exampleExtraAttribute", "Example value");
    }
}

```

Por último, se define el componente:

```impex
INSERT_UPDATE WishlistListComponent; $contentCV[unique = true]; uid[unique = true]; name          ; &componentRef
                                   ;                          ; WishlistList      ; Wishlist List ; WishlistList
```

Ya solo quedará crear el controlador de la página para poder acceder a esta.

```java
package de.hybris.training.storefront.controllers.pages;

(...)

@Controller
@RequestMapping(value = "/testPage")
public class TestPageController extends AbstractCheckoutController {

    private static final String PRODUCT_WISHLIST_CMS_PAGE = "testContentPage";
    
    @RequestMapping(method = RequestMethod.GET)
    public String testMethod(final RedirectAttributes redirectModel) {
        storeCmsPageInModel(model, getContentPageForLabelOrId(PRODUCT_WISHLIST_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PRODUCT_WISHLIST_CMS_PAGE));

        model.addAttribute("testAttribute2", "Test Value 2");
        return getViewForPage(model);
    }
    
}
```
