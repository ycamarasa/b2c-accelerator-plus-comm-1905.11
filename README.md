# Hybris Commerce Training

## First steps

* Create a new account in Gitlab with your corporate email (https://gitlab.com)
* Install SAPMachine 11 (https://sap.github.io/SapMachine)
* Install IntelliJ (https://www.jetbrains.com/idea/download)
   * Install the plugin `SAP Commerce Developers Toolset`
* Install a GIT GUI app (like SourceTree or Fork)

## Steps to run the project:

1) Fork the project to your Gitlab account
2) Clone into your computer the fork you just created (`master` branch) into a folder close to the root directory of your computer:
    * (Windows) `C:/y/training`
    * (Linux / Mac) `$HOME/y/training`
    > This will be your `{PROJECT_ROOT}`
3) Download SAP Commerce 1905 from launchpad or ask your instructor (https://launchpad.support.sap.com/#/softwarecenter/search/CXCOMM1905)
4) Copy and merge the folder `hybris` from the SAP Commerce 1905 ZIP to the root of your project
5) Open a terminal window
    * Go to `{PROJECT_ROOT}/hybris/bin/platform`
    * Execute the command:
      * (Windows) `./setantenv.bat`
      * (Linux / Mac) `. ./setantenv.sh`
    * Execute the command `ant` and press the "Enter" key to select [develop] when asked
    * Then go to `{PROJECT_ROOT}`
    * Execute the command `ant development`, it will copy all the required configuration files for this training.
    * Next go to `{PROJECT_ROOT}/hybris/bin/platform` 
    * Execute the command `ant clean initialize`, it will compile the code and initialize the DB. (It will take 12 minutes)
6) Run the server by typing into the terminal window the command (It will take 4 minutes):
   * (Windows) `./hybrisserver.bat debug`
   * (Linux / Mac) `./hybrisserver.sh debug`
   > The `debug` parameter is used to run the server in debug mode

Once the server is started, ensure the training storefront is working by loading the following URL:
> https://training.local:9002
> or
> https://localhost:9002

For the above URL to work, you must have the following entry in your `hosts` file:
```
127.0.0.1   training.local
```
